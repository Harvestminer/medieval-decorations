package harvestminer.medievaldecorations.blocks;

import harvestminer.medievaldecorations.Furniture.SeatEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.util.stream.Stream;

public class ModCustomChairBlock extends ModFurnitureBlock
{
    public ModCustomChairBlock(Properties properties)
    {
        super(properties);
        runCalculation(Stream.of(
                Block.makeCuboidShape(4, 10, 11, 12, 21, 12),
                Block.makeCuboidShape(3, 9, 3, 13, 10, 13),
                Block.makeCuboidShape(4, 0, 4, 6, 9, 6),
                Block.makeCuboidShape(4, 0, 10, 6, 9, 12),
                Block.makeCuboidShape(10, 0, 10, 12, 9, 12),
                Block.makeCuboidShape(10, 0, 4, 12, 9, 6)
        ).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get());
    }



    @SuppressWarnings("deprecation")
    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        return SHAPES.get(state.get(FACING));
    }

    @SuppressWarnings("deprecation")
    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit)
    {
        return SeatEntity.create(worldIn, pos, 0.4, player);
    }
}
