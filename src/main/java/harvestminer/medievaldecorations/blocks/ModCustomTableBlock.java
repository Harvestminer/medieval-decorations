package harvestminer.medievaldecorations.blocks;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;

import java.util.stream.Stream;

public class ModCustomTableBlock extends Block
{
    public ModCustomTableBlock(AbstractBlock.Properties properties)
    {
        super(properties);
    }

    @SuppressWarnings("deprecation")
    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        return Stream.of(
                Block.makeCuboidShape(0, 13, 0, 16, 16, 16),
                Block.makeCuboidShape(1, 0, 1, 3, 13, 3),
                Block.makeCuboidShape(1, 0, 13, 3, 13, 15),
                Block.makeCuboidShape(13, 0, 13, 15, 13, 15),
                Block.makeCuboidShape(13, 0, 1, 15, 13, 3)
        ).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
    }
}
