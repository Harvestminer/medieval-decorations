package harvestminer.medievaldecorations.data.client;

import harvestminer.medievaldecorations.MedievalDecorations;
import harvestminer.medievaldecorations.setup.ModBlocks;
import harvestminer.medievaldecorations.setup.ModTags;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;

import javax.annotation.Nullable;

public class ModBlockTagsProvider extends BlockTagsProvider
{
    public ModBlockTagsProvider(DataGenerator generatorIn, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(generatorIn, MedievalDecorations.MOD_ID, existingFileHelper);
    }

    @Override
    protected void registerTags()
    {
        getOrCreateBuilder(ModTags.Blocks.FISHSCALE_STONES_WALL_TAG).add(ModBlocks.FISHSCALE_STONES_WALL.get());
        getOrCreateBuilder(ModTags.Blocks.STACKED_PEBBLES_WALL_TAG).add(ModBlocks.STACKED_PEBBLES_WALL.get());
    }
}
