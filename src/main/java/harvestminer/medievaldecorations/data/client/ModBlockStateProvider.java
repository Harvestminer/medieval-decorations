package harvestminer.medievaldecorations.data.client;

import harvestminer.medievaldecorations.MedievalDecorations;
import harvestminer.medievaldecorations.setup.ModBlocks;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ModBlockStateProvider extends BlockStateProvider
{
    public ModBlockStateProvider(DataGenerator gen, ExistingFileHelper exFileHelper)
    {
        super(gen, MedievalDecorations.MOD_ID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels()
    {
        axisBlock(ModBlocks.SPRUCE_BORDER.get());
        trapdoorBlock(ModBlocks.SPRUCE_BORDER_TRAPDOOR.get(), modLoc("block/spruce_border_end"), false);

        simpleBlock(ModBlocks.PINK_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.RED_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.MAGENTA_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.PURPLE_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.LIGHT_BLUE_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.BLUE_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.CYAN_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.ORANGE_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.YELLOW_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.BROWN_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.SILVER_STRIPED_WALLPAPER.get());
        simpleBlock(ModBlocks.BLACK_STRIPED_WALLPAPER.get());

        simpleBlock(ModBlocks.FISHSCALE_STONES.get());
        stairsBlock(ModBlocks.FISHSCALE_STONES_STAIRS.get(), modLoc("block/fishscale_stones"));
        slabBlock(ModBlocks.FISHSCALE_STONES_SLAB.get(), modLoc("block/fishscale_stones"), modLoc("block/fishscale_stones"));
        wallBlock(ModBlocks.FISHSCALE_STONES_WALL.get(), modLoc("block/fishscale_stones"));

        simpleBlock(ModBlocks.STACKED_PEBBLES.get());
        stairsBlock(ModBlocks.STACKED_PEBBLES_STAIRS.get(), modLoc("block/stacked_pebbles"));
        slabBlock(ModBlocks.STACKED_PEBBLES_SLAB.get(), modLoc("block/stacked_pebbles"), modLoc("block/stacked_pebbles"));
        wallBlock(ModBlocks.STACKED_PEBBLES_WALL.get(), modLoc("block/stacked_pebbles"));

        simpleBlock(ModBlocks.OAK_TABLE.get());
        simpleBlock(ModBlocks.SMOOTH_OAK_TABLE.get());
        simpleBlock(ModBlocks.OAK_CHAIR.get());
    }
}
