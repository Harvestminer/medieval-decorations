package harvestminer.medievaldecorations.data.client;

import com.google.common.collect.ImmutableList;
import com.mojang.datafixers.util.Pair;
import harvestminer.medievaldecorations.setup.ModBlocks;
import harvestminer.medievaldecorations.setup.Registration;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.LootTableProvider;
import net.minecraft.data.loot.BlockLootTables;
import net.minecraft.loot.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ModLootTableProvider extends LootTableProvider
{
    public ModLootTableProvider(DataGenerator dataGeneratorIn)
    {
        super(dataGeneratorIn);
    }

    @Override
    protected List<Pair<Supplier<Consumer<BiConsumer<ResourceLocation, LootTable.Builder>>>, LootParameterSet>> getTables()
    {
        return ImmutableList.of(
                Pair.of(ModBlockLootTables::new, LootParameterSets.BLOCK)
        );
    }

    @Override
    protected void validate(Map<ResourceLocation, LootTable> map, ValidationTracker validationtracker)
    {
        map.forEach((p_218436_2_, p_218436_3_) -> {
            LootTableManager.validateLootTable(validationtracker, p_218436_2_, p_218436_3_);
        });
    }

    public static class ModBlockLootTables extends BlockLootTables
    {
        @Override
        protected void addTables()
        {
            registerDropSelfLootTable(ModBlocks.SPRUCE_BORDER.get());
            registerDropSelfLootTable(ModBlocks.SPRUCE_BORDER_TRAPDOOR.get());

            registerDropSelfLootTable(ModBlocks.PINK_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.RED_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.MAGENTA_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.PURPLE_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.LIGHT_BLUE_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.BLUE_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.CYAN_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.ORANGE_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.YELLOW_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.BROWN_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.SILVER_STRIPED_WALLPAPER.get());
            registerDropSelfLootTable(ModBlocks.BLACK_STRIPED_WALLPAPER.get());

            registerDropSelfLootTable(ModBlocks.FISHSCALE_STONES.get());
            registerDropSelfLootTable(ModBlocks.FISHSCALE_STONES_STAIRS.get());
            registerDropSelfLootTable(ModBlocks.FISHSCALE_STONES_SLAB.get());
            registerDropSelfLootTable(ModBlocks.FISHSCALE_STONES_WALL.get());

            registerDropSelfLootTable(ModBlocks.STACKED_PEBBLES.get());
            registerDropSelfLootTable(ModBlocks.STACKED_PEBBLES_STAIRS.get());
            registerDropSelfLootTable(ModBlocks.STACKED_PEBBLES_SLAB.get());
            registerDropSelfLootTable(ModBlocks.STACKED_PEBBLES_WALL.get());


            registerDropSelfLootTable(ModBlocks.OAK_TABLE.get());
            registerDropSelfLootTable(ModBlocks.SMOOTH_OAK_TABLE.get());
            registerDropSelfLootTable(ModBlocks.OAK_CHAIR.get());
        }

        @Override
        protected Iterable<Block> getKnownBlocks()
        {
            return Registration.BLOCKS.getEntries().stream()
                    .map(RegistryObject::get)
                    .collect(Collectors.toList());
        }
    }
}
