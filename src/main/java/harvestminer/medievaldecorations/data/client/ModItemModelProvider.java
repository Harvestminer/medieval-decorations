package harvestminer.medievaldecorations.data.client;

import harvestminer.medievaldecorations.MedievalDecorations;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ModItemModelProvider extends ItemModelProvider
{
    public ModItemModelProvider(DataGenerator generator, ExistingFileHelper existingFileHelper)
    {
        super(generator, MedievalDecorations.MOD_ID, existingFileHelper);
    }

    @Override
    protected void registerModels()
    {
        withExistingParent("spruce_border", modLoc("block/spruce_border"));
        withExistingParent("spruce_border_trapdoor", modLoc("block/spruce_border_trapdoor_bottom"));

        withExistingParent("pink_striped_wallpaper", modLoc("block/pink_striped_wallpaper"));
        withExistingParent("red_striped_wallpaper", modLoc("block/red_striped_wallpaper"));
        withExistingParent("magenta_striped_wallpaper", modLoc("block/magenta_striped_wallpaper"));
        withExistingParent("purple_striped_wallpaper", modLoc("block/purple_striped_wallpaper"));
        withExistingParent("light_blue_striped_wallpaper", modLoc("block/light_blue_striped_wallpaper"));
        withExistingParent("blue_striped_wallpaper", modLoc("block/blue_striped_wallpaper"));
        withExistingParent("cyan_striped_wallpaper", modLoc("block/cyan_striped_wallpaper"));
        withExistingParent("orange_striped_wallpaper", modLoc("block/orange_striped_wallpaper"));
        withExistingParent("yellow_striped_wallpaper", modLoc("block/yellow_striped_wallpaper"));
        withExistingParent("brown_striped_wallpaper", modLoc("block/brown_striped_wallpaper"));
        withExistingParent("silver_striped_wallpaper", modLoc("block/silver_striped_wallpaper"));
        withExistingParent("black_striped_wallpaper", modLoc("block/black_striped_wallpaper"));

        withExistingParent("fishscale_stones", modLoc("block/fishscale_stones"));
        withExistingParent("fishscale_stones_stairs", modLoc("block/fishscale_stones_stairs"));
        withExistingParent("fishscale_stones_slab", modLoc("block/fishscale_stones_slab"));
        withExistingParent("fishscale_stones_wall", "block/wall_inventory").texture("wall", modLoc("block/fishscale_stones"));

        withExistingParent("stacked_pebbles", modLoc("block/stacked_pebbles"));
        withExistingParent("stacked_pebbles_stairs", modLoc("block/stacked_pebbles_stairs"));
        withExistingParent("stacked_pebbles_slab", modLoc("block/stacked_pebbles_slab"));
        withExistingParent("stacked_pebbles_wall", "block/wall_inventory").texture("wall", modLoc("block/stacked_pebbles"));

        withExistingParent("oak_table", modLoc("block/oak_table"));
        withExistingParent("smooth_oak_table", modLoc("block/smooth_oak_table"));
        withExistingParent("oak_chair", modLoc("block/oak_chair"));

        ModelFile itemGenerated = getExistingFile(mcLoc("item/generated"));

        //builder(itemGenerated, "yes");
    }

    private ItemModelBuilder builder(ModelFile itemGenerated, String name)
    {
        return getBuilder(name).parent(itemGenerated).texture("layer0", "item/" + name);
    }
}
