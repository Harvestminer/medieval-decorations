package harvestminer.medievaldecorations.data.client;

import harvestminer.medievaldecorations.setup.ModBlocks;
import net.minecraft.block.Blocks;
import net.minecraft.data.*;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraftforge.common.Tags;

import java.util.function.Consumer;

public class ModRecipeProvider extends RecipeProvider
{

    public ModRecipeProvider(DataGenerator generatorIn)
    {
        super(generatorIn);
    }

    @Override
    protected void registerRecipes(Consumer<IFinishedRecipe> consumer)
    {
        ShapedRecipeBuilder.shapedRecipe(ModBlocks.OAK_TABLE.get())
                .patternLine("XXX")
                .patternLine("Y Y")
                .patternLine("Y Y")
                .key('X', Items.OAK_PLANKS)
                .key('Y', Tags.Items.RODS_WOODEN)
                .addCriterion("has_item", hasItem(Blocks.OAK_PLANKS.getBlock()))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.SMOOTH_OAK_TABLE.get())
                .patternLine("XXX")
                .patternLine("Y Y")
                .patternLine("Y Y")
                .key('X', Items.STRIPPED_OAK_LOG)
                .key('Y', Tags.Items.RODS_WOODEN)
                .addCriterion("has_item", hasItem(Blocks.STRIPPED_OAK_LOG.getBlock()))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.OAK_CHAIR.get())
                .patternLine("  Y")
                .patternLine("XXX")
                .patternLine("Y Y")
                .key('X', Items.OAK_PLANKS)
                .key('Y', Tags.Items.RODS_WOODEN)
                .addCriterion("has_item", hasItem(Blocks.OAK_PLANKS.getBlock()))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.SPRUCE_BORDER.get(), 4)
                .patternLine("XYX")
                .patternLine("ZZZ")
                .patternLine("ZZZ")
                .key('X', Items.STRIPPED_SPRUCE_LOG)
                .key('Y', Tags.Items.RODS_WOODEN)
                .key('Z', Items.SPRUCE_PLANKS)
                .addCriterion("has_item", hasItem(Blocks.SPRUCE_PLANKS.getBlock()))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.PINK_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.PINK_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.RED_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.RED_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.MAGENTA_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.MAGENTA_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.PURPLE_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.PURPLE_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.LIGHT_BLUE_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.LIGHT_BLUE_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.BLUE_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.BLUE_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.CYAN_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.CYAN_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.ORANGE_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.ORANGE_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.YELLOW_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.YELLOW_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.BROWN_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.BROWN_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.SILVER_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.LIGHT_GRAY_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.BLACK_STRIPED_WALLPAPER.get(), 16)
                .patternLine("XYX")
                .patternLine("XZX")
                .patternLine("XYX")
                .key('X', Items.BLACK_DYE)
                .key('Y', Items.PAPER)
                .key('Z', Items.CLAY_BALL)
                .addCriterion("has_item", hasItem(Items.PAPER))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.SPRUCE_BORDER_TRAPDOOR.get(), 2)
                .patternLine("XXX")
                .patternLine("XXX")
                .key('X', ModBlocks.SPRUCE_BORDER.get().asItem())
                .addCriterion("has_item", hasItem(ModBlocks.SPRUCE_BORDER.get()))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.FISHSCALE_STONES_SLAB.get(), 6)
                .patternLine("XXX")
                .key('X', ModBlocks.FISHSCALE_STONES.get().asItem())
                .addCriterion("has_item", hasItem(ModBlocks.FISHSCALE_STONES.get()))
        .build(consumer);
        ShapedRecipeBuilder.shapedRecipe(ModBlocks.FISHSCALE_STONES_STAIRS.get(), 4)
                .patternLine("X  ")
                .patternLine("XX ")
                .patternLine("XXX")
                .key('X', ModBlocks.FISHSCALE_STONES.get().asItem())
                .addCriterion("has_item", hasItem(ModBlocks.FISHSCALE_STONES.get()))
        .build(consumer);
        ShapedRecipeBuilder.shapedRecipe(ModBlocks.FISHSCALE_STONES_WALL.get(), 6)
                .patternLine("XXX")
                .patternLine("XXX")
                .key('X', ModBlocks.FISHSCALE_STONES.get().asItem())
                .addCriterion("has_item", hasItem(ModBlocks.FISHSCALE_STONES.get()))
        .build(consumer);

        ShapedRecipeBuilder.shapedRecipe(ModBlocks.STACKED_PEBBLES_SLAB.get(), 6)
                .patternLine("XXX")
                .key('X', ModBlocks.STACKED_PEBBLES.get().asItem())
                .addCriterion("has_item", hasItem(ModBlocks.STACKED_PEBBLES.get()))
        .build(consumer);
        ShapedRecipeBuilder.shapedRecipe(ModBlocks.STACKED_PEBBLES_STAIRS.get(), 4)
                .patternLine("X  ")
                .patternLine("XX ")
                .patternLine("XXX")
                .key('X', ModBlocks.STACKED_PEBBLES.get().asItem())
                .addCriterion("has_item", hasItem(ModBlocks.STACKED_PEBBLES.get()))
        .build(consumer);
        ShapedRecipeBuilder.shapedRecipe(ModBlocks.STACKED_PEBBLES_WALL.get(), 6)
                .patternLine("XXX")
                .patternLine("XXX")
                .key('X', ModBlocks.STACKED_PEBBLES.get().asItem())
                .addCriterion("has_item", hasItem(ModBlocks.STACKED_PEBBLES.get()))
        .build(consumer);

        SingleItemRecipeBuilder.stonecuttingRecipe(Ingredient.fromItems(Blocks.COBBLESTONE), ModBlocks.FISHSCALE_STONES.get())
                .addCriterion("has_item", hasItem(Blocks.COBBLESTONE)).build(consumer, "fishscale_stones_from_cobblestone");

        SingleItemRecipeBuilder.stonecuttingRecipe(Ingredient.fromItems(ModBlocks.FISHSCALE_STONES.get()), ModBlocks.FISHSCALE_STONES_SLAB.get())
                .addCriterion("has_item", hasItem(Blocks.COBBLESTONE)).build(consumer, "fishscale_stones_slab_from_fishscale_stones");
        SingleItemRecipeBuilder.stonecuttingRecipe(Ingredient.fromItems(ModBlocks.FISHSCALE_STONES.get()), ModBlocks.FISHSCALE_STONES_STAIRS.get())
                .addCriterion("has_item", hasItem(Blocks.COBBLESTONE)).build(consumer, "fishscale_stones_stairs_from_fishscale_stones");
        SingleItemRecipeBuilder.stonecuttingRecipe(Ingredient.fromItems(ModBlocks.FISHSCALE_STONES.get()), ModBlocks.FISHSCALE_STONES_WALL.get())
                .addCriterion("has_item", hasItem(Blocks.COBBLESTONE)).build(consumer, "fishscale_stones_wall_from_fishscale_stones");

        SingleItemRecipeBuilder.stonecuttingRecipe(Ingredient.fromItems(Blocks.COBBLESTONE), ModBlocks.STACKED_PEBBLES.get())
                .addCriterion("has_item", hasItem(Blocks.COBBLESTONE)).build(consumer, "stacked_pebbles_from_cobblestone");

        SingleItemRecipeBuilder.stonecuttingRecipe(Ingredient.fromItems(ModBlocks.STACKED_PEBBLES.get()), ModBlocks.STACKED_PEBBLES_SLAB.get())
                .addCriterion("has_item", hasItem(Blocks.COBBLESTONE)).build(consumer, "stacked_pebbles_slab_from_stacked_pebbles");
        SingleItemRecipeBuilder.stonecuttingRecipe(Ingredient.fromItems(ModBlocks.STACKED_PEBBLES.get()), ModBlocks.STACKED_PEBBLES_STAIRS.get())
                .addCriterion("has_item", hasItem(Blocks.COBBLESTONE)).build(consumer, "stacked_pebbles_stairs_from_stacked_pebbles");
        SingleItemRecipeBuilder.stonecuttingRecipe(Ingredient.fromItems(ModBlocks.STACKED_PEBBLES.get()), ModBlocks.STACKED_PEBBLES_WALL.get())
                .addCriterion("has_item", hasItem(Blocks.COBBLESTONE)).build(consumer, "stacked_pebbles_wall_from_stacked_pebbles");
    }
}
