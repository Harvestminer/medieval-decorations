package harvestminer.medievaldecorations.setup;

import harvestminer.medievaldecorations.blocks.ModCustomChairBlock;
import harvestminer.medievaldecorations.blocks.ModCustomTableBlock;
import harvestminer.medievaldecorations.blocks.ModCustomTrapdoorBlock;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;

import java.util.function.Supplier;

public class ModBlocks
{
    // Blocks
    public static final RegistryObject<RotatedPillarBlock> SPRUCE_BORDER = register("spruce_border", ()->
            new RotatedPillarBlock(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    // Add custom trapdoor model
    public static final RegistryObject<ModCustomTrapdoorBlock> SPRUCE_BORDER_TRAPDOOR = register("spruce_border_trapdoor", ()->
            new ModCustomTrapdoorBlock(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));

    public static final RegistryObject<Block> PINK_STRIPED_WALLPAPER = register("pink_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> RED_STRIPED_WALLPAPER = register("red_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> MAGENTA_STRIPED_WALLPAPER = register("magenta_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> PURPLE_STRIPED_WALLPAPER = register("purple_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> LIGHT_BLUE_STRIPED_WALLPAPER = register("light_blue_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> BLUE_STRIPED_WALLPAPER = register("blue_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> CYAN_STRIPED_WALLPAPER = register("cyan_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> ORANGE_STRIPED_WALLPAPER = register("orange_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> YELLOW_STRIPED_WALLPAPER = register("yellow_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> BROWN_STRIPED_WALLPAPER = register("brown_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> SILVER_STRIPED_WALLPAPER = register("silver_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));
    public static final RegistryObject<Block> BLACK_STRIPED_WALLPAPER = register("black_striped_wallpaper", ()->
            new Block(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)));


    public static final RegistryObject<Block> FISHSCALE_STONES = register("fishscale_stones", ()->
            new Block(AbstractBlock.Properties.create(Material.ROCK)
                    .hardnessAndResistance(2, 6)
                    .harvestLevel(1)
                    .sound(SoundType.STONE)));
    @SuppressWarnings("deprecation")
    public static final RegistryObject<StairsBlock> FISHSCALE_STONES_STAIRS = register("fishscale_stones_stairs", ()->
            new StairsBlock(FISHSCALE_STONES.get().getDefaultState(), AbstractBlock.Properties.from(FISHSCALE_STONES.get())));
    public static final RegistryObject<SlabBlock> FISHSCALE_STONES_SLAB = register("fishscale_stones_slab", ()->
            new SlabBlock(AbstractBlock.Properties.from(FISHSCALE_STONES.get())));
    public static final RegistryObject<WallBlock> FISHSCALE_STONES_WALL = register("fishscale_stones_wall", ()->
            new WallBlock(AbstractBlock.Properties.from(FISHSCALE_STONES.get())));


    public static final RegistryObject<Block> STACKED_PEBBLES = register("stacked_pebbles", ()->
            new Block(AbstractBlock.Properties.create(Material.ROCK)
                    .hardnessAndResistance(2, 6)
                    .harvestLevel(1)
                    .sound(SoundType.STONE)));
    @SuppressWarnings("deprecation")
    public static final RegistryObject<StairsBlock> STACKED_PEBBLES_STAIRS = register("stacked_pebbles_stairs", ()->
            new StairsBlock(STACKED_PEBBLES.get().getDefaultState(), AbstractBlock.Properties.from(STACKED_PEBBLES.get())));
    public static final RegistryObject<SlabBlock> STACKED_PEBBLES_SLAB = register("stacked_pebbles_slab", ()->
            new SlabBlock(AbstractBlock.Properties.from(STACKED_PEBBLES.get())));
    public static final RegistryObject<WallBlock> STACKED_PEBBLES_WALL = register("stacked_pebbles_wall", ()->
            new WallBlock(AbstractBlock.Properties.from(STACKED_PEBBLES.get())));

    // Furniture
    public static final RegistryObject<ModCustomTableBlock> OAK_TABLE = register("oak_table", ()->
            new ModCustomTableBlock(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)
                    .notSolid()));
    public static final RegistryObject<ModCustomTableBlock> SMOOTH_OAK_TABLE = register("smooth_oak_table", ()->
            new ModCustomTableBlock(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)
                    .notSolid()));
    public static final RegistryObject<ModCustomChairBlock> OAK_CHAIR = register("oak_chair", ()->
            new ModCustomChairBlock(AbstractBlock.Properties.create(Material.WOOD)
                    .hardnessAndResistance(2, 2)
                    .sound(SoundType.WOOD)
                    .notSolid()));

    static void register() {}

    private static <T extends Block> RegistryObject<T> registerNoItem(String name, Supplier<T> block)
    {
        return Registration.BLOCKS.register(name, block);
    }

    private static <T extends Block> RegistryObject<T> register(String name, Supplier<T> block)
    {
        RegistryObject<T> ret = registerNoItem(name, block);
        Item.Properties blockItem = new Item.Properties().group(ModItemGroup.group);

        if (name.contains("table") || name.contains("chair"))
        {
            Registration.ITEMS.register(name, () -> new BlockItem(ret.get(), blockItem.maxStackSize(16)));
            return ret;
        }

        Registration.ITEMS.register(name, () -> new BlockItem(ret.get(), blockItem));
        return ret;
    }
}
