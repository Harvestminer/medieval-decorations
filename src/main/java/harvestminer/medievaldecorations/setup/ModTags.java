package harvestminer.medievaldecorations.setup;

import harvestminer.medievaldecorations.MedievalDecorations;
import net.minecraft.block.Block;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;

public class ModTags
{
    public static final class Blocks
    {
        public static final ITag.INamedTag<Block> FISHSCALE_STONES_WALL_TAG = minecraft("walls");
        public static final ITag.INamedTag<Block> STACKED_PEBBLES_WALL_TAG = minecraft("walls");

        private static ITag.INamedTag<Block> forge(String path)
        {
            return BlockTags.makeWrapperTag(new ResourceLocation("forge", path).toString());
        }

        private static ITag.INamedTag<Block> mod(String path)
        {
            return BlockTags.makeWrapperTag(new ResourceLocation(MedievalDecorations.MOD_ID, path).toString());
        }

        private static ITag.INamedTag<Block> minecraft(String path)
        {
            return BlockTags.makeWrapperTag(new ResourceLocation("minecraft", path).toString());
        }
    }
}
