package harvestminer.medievaldecorations.setup;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class ModItemGroup
{
    public static final ItemGroup group = new ItemGroup(ItemGroup.getGroupCountSafe(),"medievaldecorations_group")
    {
        @Override
        public ItemStack createIcon()
        {
            return Registration.ITEMS.getEntries().stream().findFirst().get().get().getDefaultInstance();
        }
    };

    static void register() {}
}
