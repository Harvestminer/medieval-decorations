package harvestminer.medievaldecorations.setup;

import harvestminer.medievaldecorations.Furniture.SeatEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.RegistryObject;

public class ModEntities
{
    public static final RegistryObject<EntityType<SeatEntity>> SEAT = register("seat",
            EntityType.Builder.<SeatEntity>create((type, world) -> new SeatEntity(world), EntityClassification.MISC)
            .size(0.0F, 0.0F)
            .setCustomClientFactory((spawnEntity, world) -> new SeatEntity(world)));

    static void register() {}

    private static <T extends Entity> RegistryObject<EntityType<T>> register(String name, EntityType.Builder<T> builder)
    {
        return Registration.ENTITIES.register(name, () -> builder.build(name));
    }
}
